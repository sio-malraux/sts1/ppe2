<!DOCTYPE html>

<?php

   include '../include/header.php';
   include '../fonctions/connexion_bdd.php';
   ?>

<div class="container">
  
  <div class="panel panel-default">
      <div class="card-header">
          </br> <h2 align="center">CHOIX DU CLUB ET DE LA COMPÉTITION</h2>
          </div>
    <div class="card-footer">
              
        <br /><br />
        
        <div class="alert alert-secondary">
            <form action='GestionMaterielP2.php' method='post'>
        <h3 align="center" > Choix du club : 
        
            <!-- Connexion au bdd -->
            <select name="ChoixClub">
            <?php $club = $conn->query ("select nom from llj_kata.club"); 
            while ($nomclub = $club -> fetch()){ 
               
                echo '<option>' .$nomclub['nom']. '</option>';
                }?>
                  
            </select>
            
                
        
            <h3 align="center" >Choix de la compétition  :
        
        <select name="ChoixCompetition">
            <?php $competition = $conn->query ("select nom, num_kata, annee from llj_kata.competition");
            
            while ($nomcompetition = $competition -> fetch()){

                echo '<option value="'.$nomcompetition['num_kata'].' '.$nomcompetition['annee'].'" >' .$nomcompetition['nom'].'-'.$nomcompetition['num_kata']. '-' .$nomcompetition['annee'].'</option>';
                
            }?>
            
            
            </select>
            
          </div>
        
              <!-- Bouton Valider -->
              <div class="form-group"> 
                <div align="center" > 
                  <button type="submit" class="btn btn-success" id="IsmaLeRappeur">Valider</button> 
                </div> 
              </div>


</form>

      </div>
      
    </div>
  </div>
</div>

</body>
</html>

