<?php

include '../include/header.php';
include '../fonctions/fonctions.php';


logged_only();
?>
<br>

<div class="container">
	<div class="row">

		<div class="col-md-3 ">
		     <div class="list-group ">
              <a href="#" class="list-group-item list-group-item-action active">Paramètres</a>
                 <?php
                 if($_SESSION['role'] == 1){ //admin
                     ?>
                     <a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/jury/manage_jury.php" class="list-group-item list-group-item-action">Gestion des jurys</a>
                     <a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/compétitions/formulaire_competition.php" class="list-group-item list-group-item-action">Gestion des compétitions</a>
                     <a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/clubs/manageClub.php" class="list-group-item list-group-item-action">Gestion des clubs</a>
                     <a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/entraineur/Gestion_Entraineur.php" class="list-group-item list-group-item-action">Gestion des entraîneurs</a>
                     <a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/Sponsor/Don.php" class="list-group-item list-group-item-action">Gestion des dons</a>
                     <a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/salles/gestionReservationSalles.php" class="list-group-item list-group-item-action">Gestion des salles</a>

                     <?php

                 }else if($_SESSION['role'] == 2){ // entraineur

                     ?>
                     <a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/compétitions/BilanSaison.php" class="list-group-item list-group-item-action">Votre club</a>
                     <a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/membre/gestion_membre.php" class="list-group-item list-group-item-action">Gestion des membres</a>
                     <a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/resultat/formulaire_note.php" class="list-group-item list-group-item-action">Notation</a>
                     <a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/compétitions/inscription/inscriptionMembreCompet.php" class="list-group-item list-group-item-action">Inscription membre compétitions</a>

                     <?php
                 }else{ // membre

                     ?>
                     <a href="http://www.bts-malraux72.net/~g.taysse/ppe2_19/ppe2/projet_ppe/compétitions/BilanSaison.php" class="list-group-item list-group-item-action">Vos compétitions</a>
                 <?php
                 }
                 ?>








            </div>
		</div>
		<div class="col-md-9">
		    <div class="card">
		        <div class="card-body">
                    <?php

                    if($_SESSION['password_change'] == 1){
                        echo '<div class="alert alert-success" role="alert">
                        Votre mot de passe a bien été mis à jour !
                        <br>
                    </div>';
                        $_SESSION['password_change'] = 0;
                    }else if($_SESSION['password_change'] == 2){
                        echo '<div class="alert alert-danger" role="alert">
                        Erreur ! Vos mots de passes ne correspondent pas !
                        <br>
                    </div>';
                        $_SESSION['password_change'] = 0;
                    }


                    ?>

		            <div class="row">
		                <div class="col-md-12">
                                    <h4>Profil</h4>
		                    <hr>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-md-12">

                            <?php

                            require_once '../fonctions/connexion_bdd.php';

                            $resultat = $conn->query("SELECT membre.prenom AS prenom, membre.nom, club.nom AS club, membre.e_mail AS email, membre.tel AS tel FROM llj_kata.authentification
INNER JOIN llj_kata.membre ON authentification.login = membre.login
INNER JOIN llj_kata.club ON membre.num_club = club.num
WHERE authentification.login = '".$_SESSION['login']."'");

                            $membre = $resultat->fetch();

                            ?>

		                    <form method="post" action="modify-comptes.php">

                                <?php
                                if($_SESSION['role'] != 1) {


                                    ?>

                                    <div class="form-group row">
                                        <label for="name" class="col-4 col-form-label">Prénom</label>
                                        <div class="col-8">
                                            <span class="input-group-text"
                                                  id="basic-addon3"> <?php echo $membre['prenom'] ?> </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lastname" class="col-4 col-form-label">Nom</label>
                                        <div class="col-8">
                                            <span class="input-group-text"
                                                  id="basic-addon3"> <?php echo $membre['nom'] ?> </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lastname" class="col-4 col-form-label">Club</label>
                                        <div class="col-8">
                                            <span class="input-group-text"
                                                  id="basic-addon3"> <?php echo $membre['club'] ?> </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-4 col-form-label">Adresse mail</label>
                                        <div class="col-8">
                                            <input id="email" name="email" placeholder="<?php echo $membre['email'] ?>"
                                                   required="required" class="form-control here" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lastname" class="col-4 col-form-label">Téléphone</label>
                                        <div class="col-8">
                                            <input id="email" name="email" placeholder="<?php echo $membre['tel'] ?>"
                                                   required="required" class="form-control here" type="text">
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                               <div class="form-group row">
                                <label for="lastname" class="col-4 col-form-label">Rôle</label>
                                <div class="col-8">
                                    <?php

                                    if($_SESSION['role'] == 1){
                                        echo '<span class="input-group-text" id="basic-addon3"> Administrateur </span>';
                                    }else if($_SESSION['role'] == 2){
                                        echo '<span class="input-group-text" id="basic-addon3"> Entraîneur </span>';
                                    }else{
                                        echo '<span class="input-group-text" id="basic-addon3"> Membre </span>';
                                    }

                                    ?>
                                   </div>
                              </div>
                                <?php
                                if($_SESSION['role'] != 1) {


                                    ?>
                                    <div class="form-group row">
                                        <div class="offset-4 col-8">
                                            <button name="submit" type="submit" class="btn btn-primary">Valider</button>
                                            <button type="reset" class="btn btn-danger">Annuler</button>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </form>
		                </div>
		            </div>

                    <br>

                    <div class="row">
                        <div class="col-md-12">
                            <h4>Changement du mot de passe</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" action="modify-password.php">
                                <div class="form-group row">
                                    <label for="password" class="col-4 col-form-label">Nouveau mot de passe</label>
                                    <div class="col-8">
                                        <input id="password" name="password" placeholder="Nouveau mot de passe" class="form-control here" type="password" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="confirm_password" class="col-4 col-form-label">Confirmation mot de passe</label>
                                    <div class="col-8">
                                        <input id="confirm_password" name="confirm_password" placeholder="Confirmation du nouveau mot de passe" class="form-control here" type="password" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-4 col-8">
                                        <button name="submit" type="submit" class="btn btn-primary">Valider</button>
                                        <button type="reset" value="submit" class="btn btn-danger">Annuler</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

		        </div>
		    </div>
		</div>
	</div>

</div>
