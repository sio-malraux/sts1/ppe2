<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 06/03/2019
 * Time: 19:35
 */

include '../include/header.php';

?>
<br>

<div class="container">

    <h1>Toutes les compétitions</h1>
    <br>

    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Numéro</th>
            <th scope="col">Organisateur (Club)</th>
            <th scope="col">Nom compétition</th>
            <th scope="col">Date</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        require_once '../fonctions/connexion_bdd.php';

        $resultat = $conn->query("SELECT club.nom AS club,competition.nom,competition.num_kata, competition.date FROM llj_kata.competition
INNER JOIN llj_kata.club ON competition.num_club = club.num
ORDER BY num_kata DESC");

        while($donnees = $resultat->fetch()) {

            ?>

            <tr>
                <th scope="row"><?php echo $donnees['num_kata']; ?></th>
                <td><?php echo $donnees['club']; ?></td>
                <td><?php echo $donnees['nom']; ?></td>
                <td><?php echo $donnees['date']; ?></td>
                <?php

                $dateDuJour = date("Y-m-d");

                if($donnees['date'] < $dateDuJour == true) {
                   ?>
                    <td> <a href="classement.php?id=<?php echo $donnees['num_kata']; ?>">
                            <button class="btn btn-info my-2 my-sm-0" value="" type="submit">Voir classement</button>
                        </a></td>
                <?php
                }else{
                    ?>
                    <td>
                        <a href="participants.php?id=<?php echo $donnees['num_kata']; ?>">
                            <button class="btn btn-info my-2 my-sm-0" value="" type="submit">Voir participants</button>
                        </a>
                    </td>
                <?php

                }

                ?>

            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
