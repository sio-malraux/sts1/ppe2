<?php

   include '../include/header.php';
   require '../fonctions/fonctions.php';
   require '../fonctions/connexion_bdd.php';
           admin_only();
           
	   //La ligne 11 désactive l'affichage des erreurs 
	   //Affichage des erreurs désactivée à cause de la variable $_SESSION['compet_a_modif'] qui, lors de la 1ère visite du site,
	   //N'est pas crée avant d'appuyer sur le bouton 'Rechercher' de l'onglet 'Modification'
       ini_set('display_errors','off');
	   
	   //Sinon $_SESSION['compet_modif'] se remet toujours a 0
       if ($_SESSION['compet_modif'] == 2){
            $_SESSION['compet_modif'] = 2 ;
	   }
	   else{
		    $_SESSION['compet_modif'] = 1 ;
	   }
	   //echo $_SESSION['compet_modif'].'<br>'.$_SESSION['compet_a_modif'];
   ?>

<div class="container">
  <h2 align="center">Formulaire Compétition</h2>
  <div class="panel panel-default">
    <div class="card-header">Organisation des compétitions</div>
    <div class="card-footer">

      <ul class="nav nav-pills">
          
    
          <li class="nav-item"><a class="nav-link <?php if($_SESSION['compet_modif'] == 1){echo "active";} ?>" data-toggle="tab" href="#Creation">Création</a></li>
          <li class="nav-item"><a class="nav-link <?php if($_SESSION['compet_modif'] == 2){echo "active";} ?>" data-toggle="tab" href="#Modification">Modification</a></li>
          <li class="nav-item"><a class="nav-link <?php if($_SESSION['compet_modif'] == 3){echo "active";} ?>" data-toggle="tab" href="#Suppression">Suppression</a></li>
            
           
      </ul>
		
		<!-- Onglet Insertion-->
      <div class="tab-content">
        <div id="Creation" class="tab-pane fade <?php if($_SESSION['compet_modif'] == 1){
        echo "show active";
    } ?>">
            <form class="form-horizontal" action = "insertion_competitions.php" method="post" onsubmit="return confirm('Souhaitez-vous ajouter cette compétition ?')">
            <div class="alert alert-secondary">
              <p>Entrer le nom de la compétition :
                <input id="compet" name="compet" placeholder="Nom compétition" type="text" required /></br>  
              </p>

              <p> Choisir le club qui accueillera la compétition :
                <select id="Club" name="Club">

                  <?php $club = $conn->query("SELECT club.num as id,club.nom as nom from llj_kata.club");
                  while($cl = $club->fetch())
                  {
                  echo '<option value="' .$cl['id'].'/'.$cl['nom']. '">' .$cl['nom']. '</option>';
                  }?>
                </select>
              </p>

              <p> Entrer une date :
                <input id="annee" name="annee" placeholder="année" type="number" min="2016" max="9999" required />
                <!-- <input id="mj" name="mj" placeholder="mois-jour" type="text" required /> -->
				<input id="m" name="m" placeholder="mois" type="number" min="01" max="12" required />
				<input id="j" name="j" placeholder="jour" type="number" min="01" max="31" required />
              </p>
              <center>
                <input name="valider" type="submit" style="align: center" value="Valider"/>
                <input name="maz" type="reset" style="align: center" value="Réinitialiser"/>
              </center>
            </div>
          </form>

        </div>


        <!-- Onglet Modifier-->
        <div id="Modification" class="tab-pane fade <?php if($_SESSION['compet_modif'] == 2){echo "show active";} ?>">
            <div class="alert alert-secondary">
          <form class="form-horizontal" action = "tempo.php" method="post">
            
                
              <p> Choisir la compétition à modifier : <?php  //echo 'variable compet_modif : '.$_SESSION['compet_modif']; ?>                 
                    
                <select id="list_competition" name="list_competition">                   
                        
                  <?php $competition = $conn->query("SELECT competition.num_kata as numcompet, competition.nom as name, competition.annee as année, competition.num_club as cl FROM llj_kata.competition");
				  	  
				 
                  while($comp = $competition -> fetch())
                  {
                  echo '<option value="'.$comp['numcompet'].'*'.$comp['année'].'*'.$comp['cl'].'"';
								if($_SESSION['compet_a_modif'] != NULL){
                                    if($comp['numcompet'].'*'.$comp['année'].'*'.$comp['cl'] == $_SESSION['compet_a_modif']){echo 'selected';}
								}
                                        echo '>'.$comp['name'].' en '.$comp['année']. '</option>';
				  
                  }?>
                </select>
                  
                 <input name="Rechercher" id="Rechercher" type="submit" value="Rechercher"/>
              </p>
          </form>
                <!-- Valider la modification-->
                <form method="POST" action="modification_competitions.php">
   
                <?php
                if($_SESSION['compet_modif'] == 2){
                   //Partie affichage en fonction du club
				   
				   // Une erreur peut est génerée si on supprimme une compétition et qu'avant elle a été choisie avec le bouton "Rechercher"
                ?>
              
			  <p>Modifier le nom :                  
                  <?php 
                  $compi = $conn->query("SELECT competition.num_kata as numcompet, competition.annee as année,competition.num_club as cb, competition.nom as nam FROM llj_kata.competition");
                  echo '<input id="Modif_compet" name="Modif_compet" placeholder="Nom compétition" type="text"'; 
			  					
					while($comp = $compi -> fetch())
					{
						if($comp['numcompet'].'*'.$comp['année'].'*'.$comp['cb'] == $_SESSION['compet_a_modif']){
							echo ' value="'.$comp['nam'].'"';
						}
					}				  
				  
                  echo' required />';
                  ?>
                
              </p>

              <p> Modifier le club :
                <select id="Changement_Club" name="Changement_Club">
                   <?php 
                   $machin = explode("*",$_SESSION['compet_a_modif']);
                  $club2 = $conn->query("SELECT club.num as numclub, club.nom as naam from llj_kata.club ");
                  while($cl = $club2->fetch())
                  {                     
                  echo '<option ' ;
                         if ($cl['numclub'] == $machin[2]){echo 'selected';}
                          echo ' value="'.$cl['numclub'].'">'.$cl['naam'].'</option>';
                         
                  }?>
                </select>
              </p>

              <p> Modifier la date :			 
				 <?php 
                                 $competi = $conn->query("SELECT competition.num_kata as numcompet, competition.annee as année,competition.num_club as club, extract(MONTH from competition.date) as mois FROM llj_kata.competition");

			echo '<input id="Modif_m" name="Modif_m" placeholder="mois" type="number" min="01" max="12" value="';
			
			while($compt = $competi -> fetch())
                  {
						
						if($compt['numcompet'].'*'.$compt['année'].'*'.$compt['club'] == $_SESSION['compet_a_modif']){
							
                            echo $compt['mois'];               
						}				   
							
				  }
				  
			echo '" required />';			
			
			$compo = $conn->query("SELECT competition.num_kata as numcompet, competition.annee as année,competition.num_club as club, extract (DAY from competition.date) as jour FROM llj_kata.competition");
			echo '<input id="Modif_j" name="Modif_j" placeholder="jour" type="number" min="01" max="31"';
			
			while($compt = $compo -> fetch())
                  {
						
						if($compt['numcompet'].'*'.$compt['année'].'*'.$compt['club'] == $_SESSION['compet_a_modif']){
							
                            echo ' value="'.$compt['jour'].'"';              
						}											
				  }
				  
			echo ' required />';			
			echo '</p>';				
			?>
              
              <center>
                <input name="valider" type="submit" style="align: center" value="Valider" onclick="return confirm('Souhaitez-vous vraiment modifier cette compétition ?');"/>
                <input name="maz" type="reset" style="align: center" value="Réinitialiser"/>
                <button type="submit" name="Annuler" values="Annuler" class="btn btn-danger" onclick="return confirm('Voulez-vous annuler vos modifications ?');">Annuler</button>
              </center>
            
                 <?php          //Fin affichage en fonction du club
              }?>
               
            </form>
        </div>
	</div>
            
            
        <!--Onglet Supprimer -->
        <div id="Suppression" class="tab-pane fade <?php if($_SESSION['compet_modif'] == 3){echo "show active";} ?>">
          <form class="form-horizontal" action = "suppression_competitions.php" method="post" onsubmit="return confirm('Souhaitez-vous vraiment supprimer cette compétition ?')">
            <div class="alert alert-secondary">
              <p> Choisir la compétition à supprimer :
                <select id="list_competition2" name="list_competition2">
                  <?php $competition3 = $conn->query("SELECT competition.num_kata as numero,competition.annee as an, competition.nom as heisse FROM llj_kata.competition;");
                  while($comp3 = $competition3 -> fetch())
                  {
                  echo '<option value="'.$comp3['numero'].'*'.$comp3['an'].'"> '.$comp3['heisse']. '</option>';
                      
                  }?>
                </select>
              </p>
              
              <center>
                <input name="valider" type="submit" style="align: center" value="Valider"/>
                <input name="maz" type="reset" style="align: center" value="Réinitialiser"/>
              </center>
            </div>
          </form>

        </div>

    </div>
  </div>
</div>

</body>
</html>


