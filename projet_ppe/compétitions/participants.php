<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 03/03/2019
 * Time: 15:09
 */


include '../include/header.php';


$compet_id = $_GET['id'];

?>
<br>
<div class="container">

    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Informations</h5>
                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Special title treatment</h5>
                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        </div>
    </div>
    <br>


    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Club</th>
            <th scope="col">Prénom</th>
            <th scope="col">Nom</th>
        </tr>
        </thead>
        <tbody>
        <?php
        require_once '../fonctions/connexion_bdd.php';

        $resultat = $conn->query("SELECT club.nom AS club, membre.prenom AS prenom, membre.nom AS nom FROM llj_kata.competition 
INNER JOIN llj_kata.inscription ON competition.num_kata = inscription.num_kata 
INNER JOIN llj_kata.membre ON inscription.licence_m = membre.licence_m
INNER JOIN llj_kata.club ON membre.num_club = club.num
WHERE competition.num_kata=".$compet_id."
ORDER BY prenom DESC");


        while($donnees = $resultat->fetch()) {


            ?>

            <tr>
                <td><?php echo $donnees['club']; ?></td>
                <td><?php echo $donnees['prenom']; ?></td>
                <td><?php echo $donnees['nom']; ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>


