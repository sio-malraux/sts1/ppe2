<?php

if(!empty($_POST) && !empty($_POST['login']) && !empty($_POST['password'])){
    require_once 'connexion_bdd.php';
    $req = $conn->prepare('SELECT * FROM llj_kata.authentification WHERE (login = :login)');
    $req->execute(['login' => $_POST['login']]);
    $user = $req->fetch();

    if($user == null){
        session_start();

        $_SESSION['connexion'] = 1;
        header("Location: ../connexion.php");
        exit();
    }elseif(password_verify($_POST['password'], $user['password'])){

        $requete = $conn->query("SELECT membre.licence_m AS licence, membre.prenom AS prenom, membre.nom AS nom FROM llj_kata.authentification
INNER JOIN llj_kata.membre ON authentification.login = membre.login
WHERE authentification.login='".$user['login']."'");
        $login = $requete->fetch();

        session_start();
        $_SESSION['auth'] = $user;
        $_SESSION['pseudo'] = $login['prenom']." ".$login['nom'];
        $_SESSION['login'] = $user['login'];
        $_SESSION['role'] = $user['role'];
        $_SESSION['password_change'] = 0;
        $_SESSION['membre'] = 0;
        $_SESSION['idconnexion'] = "";
        $_SESSION['club_modification']=0;
        $_SESSION['compet_modif']=0;
        $_SESSION['licence'] = $login['licence'];
        header("Location: ../compte/parametre.php");
        exit();
    }else{
        session_start();
        $_SESSION['connexion'] = 1;
        header("Location: ../connexion.php");
        exit();
    }
}
?>
