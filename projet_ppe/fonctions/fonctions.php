<?php

function admin_only(){
    if(session_status() == PHP_SESSION_NONE){
        session_start();
    }
    if(!isset($_SESSION['auth']) || $_SESSION['role'] != 1){
        header('Location: ../index.php');
        exit();
    }

}

function entraineur_only(){
    if(session_status() == PHP_SESSION_NONE){
        session_start();
    }
    if(!isset($_SESSION['auth']) || $_SESSION['role'] != 2){
        header('Location: ../index.php');
        exit();
    }
}

function logged_only(){
    if(session_status() == PHP_SESSION_NONE){
        session_start();
    }
    if(!isset($_SESSION['auth'])){
        header('Location: ../index.php');
        exit();
    }
}

function unlogged_only(){
    if(session_status() == PHP_SESSION_NONE){
        session_start();
    }
    if(isset($_SESSION['auth'])){
        header('Location: ../index.php');
        exit();
    }
}

function genererChaineAleatoire($longueur = 15)
{
    $listeCar = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $chaine = '';
    $max = mb_strlen($listeCar, '8bit') - 1;
    for ($i = 0; $i < $longueur; ++$i) {
        $chaine .= $listeCar[random_int(0, $max)];
    }
    return $chaine;
}

?>