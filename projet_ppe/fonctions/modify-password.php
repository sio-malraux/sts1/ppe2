<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 02/03/2019
 * Time: 15:57
 */

include 'fonctions.php';

logged_only();
if(!empty($_POST)){

    if(empty($_POST['password']) || $_POST['password'] != $_POST['confirm_password']){
        header('Location: ../compte/parametre.php');
        $_SESSION['password_change'] = 2;
    }else{
        $user_id = $_SESSION['pseudo'];
        $password= password_hash($_POST['password'], PASSWORD_BCRYPT);
        require_once 'connexion_bdd.php';
        $req = $conn ->prepare('UPDATE llj_kata.authentification SET password = ? WHERE login = ?');
        $req->execute([$password,$user_id]);
        $_SESSION['password_change'] = 1;
        header('Location: ../compte/parametre.php');
    }
}

?>