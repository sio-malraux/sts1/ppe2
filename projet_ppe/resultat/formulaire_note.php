<!DOCTYPE html>

<?php

include '../include/header.php';
require '../fonctions/connexion_bdd.php';


?>

<div class="container">
    <h2  align="center">Formulaire Note</h2>
    <div class="panel panel-default">
    <div class="card-header">Notation des Candidats</div>
    <div class="card-footer">
      
    <ul class="nav nav-pills">
    <li class="nav-item"><a class="nav-link <?php if($_SESSION['jury_modification'] == 1){echo "active";}?>" data-toggle="tab" href="#Saisir">Saisir</a></li>
    <li class="nav-item"><a class="nav-link <?php if($_SESSION['jury_modification'] == 2){echo "active";}?>" data-toggle="tab" href="#Modifier">Modifier</a></li>
    <li class="nav-item"><a class="nav-link <?php if($_SESSION['jury_modification'] == 3){echo "active";}?>" data-toggle="tab" href="#Supprimer">Supprimer</a></li>
    </ul>

      
    <div class="tab-content">
          
    <!-- Onglet saisir -->
    <div id="Saisir" class="tab-pane fade <?php    if($_SESSION['jury_modification'] == 1){echo "show active";}?>">
    <div class="alert alert-secondary">
                
    <!-- <p> Rentrer votre numero de d'entraineur:-->
                <!--<select id="list_entraine" name="list_entraine">-->
                  <?php 
		  $_SESSION['licence']=7;
                  /*$entraine = $conn->query("SELECT licence_pro, prenom, nom FROM llj_kata.entraineur;");
                  while($entr = $entraine -> fetch())
                  {
                  echo '<option value="'.$entr['licence_pro'].'" >'.$entr['prenom'].' '.$entr['nom']. '</option>';
                  }*/?>
                <!--</select>-->
              <!-- </p> -->
              
              
              <!-- Formulaire pour validation de la competition -->
              <form class="form-horizontal" method="post" action = "insert_note.php">
              <p> Choisir la compétition dans laquelle vous jugez :
                <select id="list_competition" name="list_competition">
                  <?php $competition = $conn->query("SELECT nom, competition.annee, competition.num_kata
                                                    from llj_kata.jury
                                                    inner join llj_kata.competition
                                                    on jury.num_kata = competition.num_kata
                                                    and jury.annee = competition.annee
                                                    WHERE jury.licence_pro = ".$_SESSION['licence'].";");
                  while($comp = $competition -> fetch())
                  {
			if(isset($_SESSION['select_compet_insert'])) { 
			  // si on a validé la compet pour recherche des inscrits il faut rester sur la compet sélectionnée
	                  	echo '<option value="'.$comp['num_kata'].'/'.$comp['annee'].'"';
				if ($_SESSION['select_compet_insert']==($comp['num_kata'].'/'.$comp['annee']))
                   	        {
                          	   echo 'selected';
                         	}
				echo '>'.$comp['nom']. '</option>';
			}
			else {
				echo '<option value="'.$comp['num_kata'].'/'.$comp['annee'].'">'.$comp['nom']. '</option>';
			}
                  } // fin while ?>
                </select>
                  <input type="submit" value="Valider" name="ValiderCompet">
              </p>
              </form>
              
              
              <!-- Formulaire de validation de l'onglet saisir-->
    <form class="form-horizontal" method="post" action = "insert_note.php">
<?php if(isset($_SESSION['select_compet_insert']))  // si "az"
{
    $listes = explode('/', $_SESSION['select_compet_insert']);
    ?>
    <p> Choisir le candidat :
    <select id="list_candidat" name="list_candidat">
<?php
    $candida = $conn->query("SELECT membre.licence_m as id, membre.nom as nom, membre.prenom as prenom 
                                            FROM llj_kata.inscription 
                                            INNER JOIN llj_kata.membre 
                                            ON membre.licence_m = inscription.licence_m
                                            WHERE num_kata =".$listes[0]."and annee = ".$listes[1].";");
    while($cand = $candida -> fetch())
    {
        echo '<option value="'.$cand['id'].'">'.$cand['nom'].' '.$cand['prenom'].'</option>';
    }?>
    </select>
    </p>
              
              
    <p> Entrez une note :
    <input id=score name="score" placeholder="note" type="number" min="0" max="30" value="15" required /> </br></br>
    </p>

<?php
}//fin si "az"
    ?>
              
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="Btn_insert" id="Btn_insert" class="btn btn-success">Valider</button>
    </div>
    </div>
    </div>
    </form>
    </div>
        
    <!-- Onglet modifier -->
    <div id="Modifier" class="tab-pane fade <?php    if($_SESSION['jury_modification'] == 2){echo "show active";}?>">
    <div class="alert alert-secondary">
                
    <p> Rentrer votre numero de d'entraineur:
                <!--<select id="list_entraine" name="list_entraine">-->
                  <?php 
		  $_SESSION['licence']=7;
                  /*$entraine = $conn->query("SELECT licence_pro, prenom, nom FROM llj_kata.entraineur;");
                  while($entr = $entraine -> fetch())
                  {
                  echo '<option value="'.$entr['licence_pro'].'" >'.$entr['prenom'].' '.$entr['nom']. '</option>';
                  }*/?>
                <!--</select>-->
              </p>
              
              <!-- Formulaire pour validation de la competition -->
              <form class="form-horizontal" method="post" action = "modif_note.php">
              <p> Choisir la compétition dans laquelle vous jugez :
                <select id="competition" name="competition">
                  <?php $competitio = $conn->query("SELECT nom, competition.annee, competition.num_kata
                                                    from llj_kata.jury
                                                    inner join llj_kata.competition
                                                    on jury.num_kata = competition.num_kata
                                                    and jury.annee = competition.annee
                                                    WHERE jury.licence_pro = ".$_SESSION['licence'].";");
                  while($compe = $competitio -> fetch())
                  {
			if(isset($_SESSION['select_compet_modif'])) { 
			  // si on a validé la compet pour recherche des inscrits il faut rester sur la compet sélectionnée
	                  	echo '<option value="'.$compe['num_kata'].'/'.$compe['annee'].'"';
				if ($_SESSION['select_compet_modif']==($compe['num_kata'].'/'.$compe['annee']))
                   	        {
                          	   echo 'selected';
                         	}
				echo '>'.$compe['nom']. '</option>';
			}
			else {
				echo '<option value="'.$compe['num_kata'].'/'.$compe['annee'].'">'.$compe['nom']. '</option>';
			}
                  } // fin while ?>
                </select>
                  <input type="submit" value="Valider" name="ValiderCompetition">
              </p>
              </form>
              
              
              <!-- Formulaire de validation de l'onglet modification-->
    <form class="form-horizontal" method="post" action = "modif_note.php">
<?php if(isset($_SESSION['select_compet_modif']))  // si "az"
{
    $liste = explode('/', $_SESSION['select_compet_modif']);
    ?>
    <p> Choisir le candidat :
    <select id="candidat" name="candidat">
<?php
    $candidat = $conn->query("SELECT membre.licence_m as id, membre.nom as nom, membre.prenom as prenom 
                                            FROM llj_kata.membre 
                                            INNER JOIN llj_kata.resultat 
                                            ON resultat.licence_m = membre.licence_m
                                            WHERE num_kata =".$liste[0]."and annee = ".$liste[1].";");
    while($candi = $candidat -> fetch())
    {
        echo '<option value="'.$candi['id'].'">'.$candi['nom'].' '.$candi['prenom'].'</option>';
    }?>
    </select>
    </p>
              
              
    <p> Entrez une note :
    <input id=score name="score" placeholder="note" type="number" min="0" max="30" value="15" required /> </br></br>
    </p>

<?php
}//fin si "az"
?>
              
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="Btn_modif" id="Btn_modif" class="btn btn-success">Valider</button>
    </div>
    </div>
    </div>
    </form>
    </div>

    <!-- Onglet supprimer -->
    <div id="Supprimer" class="tab-pane fade <?php    if($_SESSION['jury_modification'] == 3){echo "show active";} ?>">
          
    <div class="alert alert-secondary">
    <p> Rentrer votre numero de d'entraineur:
                <!--<select id="list_entraineur2" name="list_entraineur2">-->
                  <?php 
                  $_SESSION['licence']=7;
                  /*$entraine = $conn->query("SELECT licence_pro, prenom, nom FROM llj_kata.entraineur;");
                  while($entr = $entraine -> fetch())
                  {
                  echo '<option value="'.$entr['licence_pro'].'" >'.$entr['prenom'].' '.$entr['nom']. '</option>';
                  }*/?>
                <!--</select>-->
              </p>
              
              <!-- Formulaire pour la Recherche -->
              <form class="form-horizontal" method="post" action = "supp_note.php">
              <p> Choisir la compétition dans laquelle vous jugez :
                <select id="Competition2" name="Competition2">
                  <?php $competition2 = $conn->query("SELECT nom, competition.annee, competition.num_kata
                                                    from llj_kata.jury
                                                    inner join llj_kata.competition
                                                    on jury.num_kata = competition.num_kata
                                                    and jury.annee = competition.annee
                                                    WHERE jury.licence_pro = ".$_SESSION['licence'].";");
                  while($compet = $competition2 -> fetch())
                  {
                  	if(isset($_SESSION['select_compet_suppr'])) { 
			  // si on a validé la compet pour recherche des inscrits il faut rester sur la compet sélectionnée
	                  	echo '<option value="'.$compet['num_kata'].'/'.$compet['annee'].'"';
				if ($_SESSION['select_compet_suppr']==($compet['num_kata'].'/'.$compet['annee']))
                   	        {
                          	   echo 'selected';
                         	}
				echo '>'.$compet['nom']. '</option>';
			}
			else {
				echo '<option value="'.$compet['num_kata'].'/'.$compet['annee'].'">'.$compet['nom']. '</option>';
			}
                  }?>
                </select>
              <input type="submit" value="Valider" name="ValiderCompet">
              </p>
              </form>
              
              <!-- Formulaire de validation pour l'onglet supprimer-->
    <form class="form-horizontal" method="post" action = "supp_note.php">
<?php if(isset($_SESSION['select_compet_suppr']))//debut si "qw"
{
    $list = explode('/', $_SESSION['select_compet_suppr']);
    //echo 'Test : '.$list[0].' - '.$list[1];
    $_SESSION["num_kata"]=$list[0];
    $_SESSION["annee"]=$list[1];
    ?>
    <p> Choisir le candidat :
    <select id="Candid" name="Candid">
<?php
    $candidats = $conn->query("SELECT membre.licence_m as id, membre.nom as nom, membre.prenom as prenom 
                                            FROM llj_kata.membre 
                                            INNER JOIN llj_kata.resultat 
                                            ON resultat.licence_m = membre.licence_m
                                            WHERE num_kata =".$list[0]."and annee = ".$list[1].";");
    while($candi = $candidats -> fetch())
    {
        echo '<option value="'.$candi['id'].'">'.$candi['nom'].' '.$candi['prenom'].'</option>';
    }?>
    </select>
    </p>
<?php
}//fin si "qw"
?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" id="Btn_suppr" name="Btn_suppr" class="btn btn-success">Valider</button>
    </div>
    </div>
    </div>
    </form>
          
    </div>
    <!--Fin de l'onglet Supprimer -->
    <form class="form-horizontal" method="post" action = "generPDF.php">
        <center><button type="submit" id="Btn_PDF" name="Btn_PDF" class="btn btn-warning">PDF</button></center>
    </form>
      </div>
      
    </div>
  </div>
</div>

</body>
</html>

