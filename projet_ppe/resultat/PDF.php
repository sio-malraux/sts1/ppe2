<?php
session_start();

require '../fonctions/connexion_bdd.php';

$liste = explode('/', $_POST['competition']);
$req = "SELECT membre.licence_m as id, note, membre.nom as nom, membre.prenom as prenom 
        FROM llj_kata.membre 
        INNER JOIN llj_kata.resultat 
        ON resultat.licence_m = membre.licence_m
        WHERE num_kata =".$liste[0]." and annee = ".$liste[1]." and licence_pro=".$_SESSION['licence'].";";
$candidat = $conn->query($req);
$i=0;
while($candi = $candidat -> fetch())
    {
        $tab[$i]['nom']=$candi['nom'];
         $tab[$i]['prenom']=$candi['prenom'];
          $tab[$i]['note']=$candi['note'];
        $i++;
    }
    
    
require __DIR__.'/../vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;

ob_start();
?>

<page> <h2 align="center">Les notes pour la competition <?php echo $liste[0] ?></h2>
<div class="table-responsive">
    <table bgcolor="#DCDCDC" border="0.2" align="center" class="table table-striped table-sm" >
        <thead>
            <tr>
                <th style="vertical-align:middle;" width="200" height="20" align="center" >Nom</th>
                <th style="vertical-align:middle;" width="200" height="20" align="center">Prenom</th>
                <th style="vertical-align:middle;" width="200" height="20" align="center">Note</th>
            </tr>
        </thead>


        <?php
        $fin=$i;
        $j=0;
        while($j<$fin){
            ?>
            <tr style="vertical-align:middle;" width="200" height="30" align="center">
                <td style="vertical-align:middle;" width="200" height="20" align="center" >
                    <?php echo $tab[$j]['nom'] ?>
                    </td>
                <td style="vertical-align:middle;" width="200" height="20" align="center">
                    <?php echo $tab[$j]['prenom'] ?>
                </td>
                <td style="vertical-align:middle;" width="200" height="20" align="center">
                    <?php echo $tab[$j]['note'] ?>
                </td>
            </tr>
            <?php
            $j++;
        }
        ?>
    </table>
</div>



    
    </page>

 <?php 
$content = ob_get_clean();


$html2pdf = new Html2Pdf();
$html2pdf->writeHTML($content);
$html2pdf->output();

?>

