<?php
include '../include/header.php';
include '../fonctions/connexion_bdd.php';
function entraineur_only(){
    if(session_status() == PHP_SESSION_NONE){
        session_start();
    }
    if(!isset($_SESSION['auth']) || $_SESSION['role'] != 2){
        header('Location: ../index.php');
        exit();
    }
}
?>

<style>
    form {
  /* Uniquement centrer le formulaire sur la page */
  margin: 100px;
  width: 1000px;
  /* Encadré pour voir les limites du formulaire */
  padding: 4em;
  border: 1px solid #CCC;
  border-radius: 4em;
}

</style>

 
<form class="needs-validation" method="POST" action="editionLicence.php">
               <h4 align=center>Judo Jujitsu</h4>
          
               
               <div class="col-md-3 mb-3">
                   <label for="licence">N° Licence :</label>
                  
                <select class="custom-select d-block w-100" id="licence"  name="licence"  required>
                    <option value="" > </option>
<?php
$requeteLicence="SELECT licence_m FROM llj_kata.membre ORDER BY licence_m";
$resultatLicence= $conn ->query($requeteLicence);

while($numLicence = $resultatLicence -> fetch()){

    
  if (isset($_POST['licence'])) {  ?>
         <option value="<?php echo $numLicence['licence_m']; ?>" <?php  if ($_POST['licence']==$numLicence['licence_m']) echo 'selected'; ?> ><?php echo $numLicence['licence_m']; ?></option>
        
    <?php   }
        else
        {  ?>
    <option value="<?php echo $numLicence['licence_m']; ?>"><?php echo $numLicence['licence_m']; ?></option>
<?php
}// fin else
} // fin while
?>  
                
                </select>
                        <input type="submit" value="Rechercher">
                       
              </div>
</form>
<form method="POST" action="edition_form.php">
              <div class="mb-3">
<?php
if (isset($_POST["licence"]))
{
     
$requete="SELECT membre.licence_m,membre.prenom, membre.nom as nom, club.nom AS club, membre.num_club, membre.date_naiss FROM llj_kata.membre INNER JOIN llj_kata.club ON membre.num_club = club.num WHERE licence_m=".$_POST['licence'].";";
$resultat= $conn->query($requete);
$x=$resultat->fetch();

}
?>
                <div class="mb-3">
                <label for="licence">N° Licence :</label>
                <input readonly type="text" class="form-control" name="licence" id="licence" placeholder="" value="<?php if (isset($_POST["licence"]))
{ echo $x['licence_m'];} ?> " required>
                </div>
                <label for="prenom">Prénom :</label>
                <input readonly type="text" class="form-control" name="prenom" id="prenom" placeholder="" value="<?php if (isset($_POST["licence"]))
{ echo $x['prenom'];} ?> " required>
              </div>
            <div class="mb-3">
                <label for="nom">Nom :</label>
                <input readonly type="text" class="form-control" name="nom" id="nom" placeholder="" value="<?php if (isset($_POST["licence"]))
{ echo $x['nom'];} ?>" required>
              </div>
            <div class="mb-3">
              <label for="club">Club :</label>
              <input readonly type="text" class="form-control" name="club" id="club" value="<?php if (isset($_POST["licence"]))
{ echo $x['club'];} ?>" required>
              <div class="invalid-feedback">
              </div>
            </div>
            <div class="mb-3">
              <label for="n°club">N° Club :</label>
              <input readonly type="text" class="form-control" name="numClub" id="numClub" value="<?php if (isset($_POST["licence"]))
{ echo $x['num_club'];} ?>" required>
              <div class="invalid-feedback">
              </div>
            </div>
            <div class="mb-3">
              <label for="dateNaissance">Date de naissance :</label>
              <input readonly type="date" class="form-control" name="dateNaiss" id="dateNaiss" value="<?php if (isset($_POST["licence"]))
{ echo $x['date_naiss'];} ?>" required>
              <div class="invalid-feedback">
              </div>
            </div>
    
            <input type="submit" value="Editer">
          </form>

