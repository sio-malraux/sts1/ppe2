<?php


include '../vendor/autoload.php';
  
use Spipu\Html2Pdf\Html2Pdf; 

try {
    ob_start();
    
   
$content=' 
<page format="100x200" orientation="L" backcolor="#AAAACC" style="font: arial;"> 
	<div style="rotate: 90; position: absolute; width: 100mm; height: 4mm; left: 195mm; top: 0; font-style: italic; font-weight: normal; text-align: center; font-size: 2.5mm;">
        	Licence 2018 / 2019
    	</div>
	 <table style="width: 99%;border: none;" cellspacing="4mm" cellpadding="0">
        <tr>
		<td colspan="3" style="width: 100%">
             	    <div class="zone" style="height: 34mm;position: relative;font-size: 5mm;">
                    <div style="position: absolute; right: 3mm; top: 13mm; text-align: right; font-size: 4mm; ">
                        N° de licence : <b>'.$_POST['licence'].'</b><br>
                    </div>
			<div style="position: absolute; right: 3mm; top: 20mm; text-align: right; font-size: 4mm; ">
                        <img src="../images/identite.png" alt="logo" width=150 height=100 style="margin-top: 3mm; margin-left: 20mm"> 
                    </div> 
                    <div style="position: absolute; right: 3mm; top: 8mm; text-align: right; font-size: 4mm; ">
                        <b>'.$_POST['prenom'].' '.$_POST['nom'].'</b><br>
                        
                    </div>
                     <div style="position: absolute; right: 3mm; bottom: 10mm; text-align: right; font-size: 4mm; "> 
                         N° du club : <b>'.$_POST['numClub'].'</b> <br> Nom du club : <b>'.$_POST['club'].'</b><br>
                        Date de naissance : <b>'.$_POST['dateNaiss'].' </b><br>
                        
                    </div>
                    <img src="../images/ffj.png" alt="logo" width=100px height=150px style="margin-top: 3mm; margin-left: 20mm">
                </div>
            </td>
             
        </tr>
        
    </table>
            </page>';
 
    //$content = ob_get_clean();
    $html2pdf = new Html2Pdf('P', 'A4', 'fr');
    $html2pdf->writeHTML($content);
    $html2pdf->output('editionLicence.pdf');
    //ob_end_clean();
} catch (Html2PdfException $e) {
   $html2pdf->clean();
    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}
?>
