<?php

include '../include/header.php';
include '../fonctions/connexion_bdd.php';

$membre = $conn->query("SELECT club.nom AS nom, club.num AS num, club.rue AS rue, club.ville AS ville, club.tel AS tel, club.mail AS mail, club.nom_president AS nom_president FROM llj_kata.club ORDER BY club.num ASC ");
//SELECT club.nom, club.num, club.rue, club.ville, club.nom, club.tel, club.mail, club.nom_president, count(membre.licence_m) AS licence FROM llj_kata.club INNER JOIN llj_kata.membre ON membre.num_club=club.num GROUP BY club.num ORDER BY club.num ASC ");



?>

<div class ="container">

    <br>

    <h1>Clubs</h1>
    <h5><i>Tout les clubs existants</i></h5>
    <hr>
    <?php
    $retour_ligne= -1;

    while ($affiche = $membre->fetch()){

        $retour_ligne = $retour_ligne +1;

        if($retour_ligne == 0){
            echo '<div class="card-deck">';
        }

        $nbr_membre = $conn->query("SELECT count (*) AS nbr FROM llj_kata.club INNER JOIN llj_kata.membre ON membre.num_club=club.num WHERE club.nom='" . $affiche['nom'] . "'");
        $affichage_membre = $nbr_membre->fetch();
        ?>


        <div class="card">
            <div class="card-body">
                <h5 class="title" align="center"><strong><?php echo $affiche['nom']; ?></strong></h5>
                <hr>
                <p class="card-text"><U> Informations :</U>
                    <br>
                    - Nombre Membre : <?php  echo $affichage_membre['nbr'] ?>
                    <br>
                    - Adresse :

                    <br>
                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp

                    Ville : <?php echo $affiche['ville'];?>
                    <br>
                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                    Rue : <?php echo $affiche['rue'];?>
                <hr>
                <U> Contactez-nous : </u>
                <br>
                &nbsp &nbsp &nbsp
                Tél : <?php echo $affiche['tel'];?>
                <br>
                &nbsp &nbsp &nbsp
                Mail : <?php echo $affiche['mail'];?>
                <hr>
                <u> Dirigeant :</u> <?php echo $affiche['nom_president']; ?>
                <hr>
                </p>
                <div class="col text-center">
                    <a href="membres/index.php?id=<?php echo $affiche['num'] ?>" >
                    <button class="btn btn-info my-2 my-sm-0" name="" value="" type="submit">
                        Voir membre
                    </button>
                    </a>
                </div>

            </div>
            <div class="card-footer">
                <small class="text-muted">  </small>
            </div>


        </div>
        <?php

        if($retour_ligne == 2){
            echo '</div>';
            echo '<br><hr><br>';
            $retour_ligne = -1;
        }
    }
    ?>

</div>
