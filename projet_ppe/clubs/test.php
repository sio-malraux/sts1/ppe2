<?php

session_start();
/*
 * Générer un PDF à partir d'une base de données
 */
require '../fonctions/connexion_bdd.php';
include '../vendor/autoload.php';
//require_once dirname(__FILE__).'/../vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

ob_start();
?>

<page backtop="5%" backbottom="5%" backleft="5%" backright="5%">

    <?php

    $membre = $conn->query("SELECT club.nom AS nom, club.num AS num, club.rue AS rue, club.ville AS ville, club.tel AS tel, club.mail AS mail, club.nom_president AS nom_president FROM llj_kata.club ORDER BY club.num ASC ");
    //SELECT club.nom, club.num, club.rue, club.ville, club.nom, club.tel, club.mail, club.nom_president, count(membre.licence_m) AS licence FROM llj_kata.club INNER JOIN llj_kata.membre ON membre.num_club=club.num GROUP BY club.num ORDER BY club.num ASC ");



    ?>

    <div class ="container">

        <br>

        <h1>Les Clubs</h1>
        <h5><i>Tout les clubs existants</i></h5>
        <hr>
        <?php
        $retour_ligne= -1;

        while ($affiche = $membre->fetch()){

            $retour_ligne = $retour_ligne +1;

            if($retour_ligne == 0){
                echo '<div class="card-deck">';
            }

            $nbr_membre = $conn->query("SELECT count (*) AS nbr FROM llj_kata.club INNER JOIN llj_kata.membre ON membre.num_club=club.num WHERE club.nom='" . $affiche['nom'] . "'");
            $affichage_membre = $nbr_membre->fetch();
            ?>


            <div class="card">
                <div class="card-body">
                   <div class="table-responsive">
                       <table bgcolor="#a9a9a9" border="0.3" align="center" class="table table-striped table-m">
                           <tr>
                    <th style="vertical-align: middle" width="600" height="30" align="center"><b><?php echo $affiche['nom']; ?></b></th>
                           </tr>
                    <tr>
                    <th width="600" align="righ"><U> Informations :</U>
                        <br>
                        - Nombre Membre : <?php  echo $affichage_membre['nbr'] ?>
                        <br>
                        - Adresse :
                        <br>

                        <div style="margin-left:30px">Ville : <?php echo $affiche['ville'];?></div>
                        <div style="margin-left:30px">Rue : <?php echo $affiche['rue'];?></div>
                        <br>
                           </th>
                    </tr>
                           <tr >
                               <th width="600" align="left">
                    <U> Contactez-nous : </u>
                    <br>

                                   <div style="margin-left:30px">Tél : <?php echo $affiche['tel'];?></div>

                                   <div style="margin-left:30px">Mail : <?php echo $affiche['mail'];?></div>
                               </th>
                           </tr>
                           <tr align="left" height="30">
                    <th> Dirigeant : <?php echo $affiche['nom_president']; ?> </th>
                    </tr>
                           <br><br>
                   </table>
                </div>
                </div>
                <hr>
                <div class="card-footer">
                    <small class="text-muted">  </small>
                </div>
            </div>
            <?php

            if($retour_ligne == 2){
                echo '</div>';
                echo '<br><hr><br>';
                $retour_ligne = -1;
            }
        }
        ?>

    </div>
</page>

<?php
$content = ob_get_clean();

$html2pdf = new Html2Pdf('P','A4', 'fr', 'true', 'UTF-8');
$html2pdf->writeHTML($content);
$html2pdf->output();

?>


<!-- Faire un onglet choix d'information sur un club (liste déroulante) avec résultat de la demande en PDF -->
